package windowsPathEnv

import (
	te "testing"
	"os"
	"fmt"
)

func Example() {

	fmt.Println(Resolve("%GOPATH%/pkg/stu-b-doo"))
	fmt.Println(Resolve("%USERPROFILE%/Documents"))
}

func TestDev(t *te.T) {

	testStr := "%USERPROFILE%/Documents"

	fmt.Println(Resolve(testStr))
}

func TestResolve(t *te.T) {

	gp := os.Getenv("gopath")
	up := os.Getenv("userprofile")

	// test cases
	cases := []struct{in, exp string}{
		// no envs
		{"path/to/dir", "path/to/dir"},
		// no second %
		{"%gopath/aslfkj", "%gopath/aslfkj"},
		// env var not full element
		{"%gopath%trailing/next", "%gopath%trailing/next"},
		// probably not an env var
		{"%banana%/split", "%banana%/split" },
		// genuine replacement
		{"%gopath%/pkg/stu-b-doo", gp + "\\pkg\\stu-b-doo"},
		{"%userprofile%/Documents", up + "\\Documents"},
	}

	// run test
	for _, c := range cases {

		act := Resolve(c.in)
		if act != c.exp {
			t.Fatalf("In %v, act %v, exp %v\n", c.in, act, c.exp)
		}
	}
}

